package chat;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

public class ClientHandler implements Runnable{
    public static ArrayList<ClientHandler> clientHandlers = new ArrayList<>();
    private Socket socket;
    private BufferedReader bufferedReader;
    private BufferedWriter bufferedWriter;
    private String clienUsername;
    private boolean salir = false;

    public ClientHandler(Socket socket) {
        try {
            this.socket = socket;
            this.bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            this.clienUsername = bufferedReader.readLine();
            while (clientHandlers.size() >= 2){
                System.out.println(clientHandlers.size());
            }
            clientHandlers.add(this);
            broadcastMessage("SERVER: "+clienUsername+ " join the chat");
        } catch (IOException e) {
            closeEverything(socket,bufferedReader,bufferedWriter);
        }
    }

    @Override
    public void run() {
        String messageFromClient;
        while (socket.isConnected()){
            try{
                messageFromClient = bufferedReader.readLine();
                if (messageFromClient.equals("adios")){
                    clientHandlers.remove(this);
                    closeEverything(socket,bufferedReader,bufferedWriter);
                }
                if (messageFromClient != null){
                    Server.mensajesEnviados.add(new MensajesEnviados(this.clienUsername,messageFromClient));
                    broadcastMessage(clienUsername + ": " +messageFromClient);
                }
            } catch (Exception e) {
//                closeEverything(socket,bufferedReader,bufferedWriter);
                break;
            }
        }
    }

    private void broadcastMessage(String s) {
        for (ClientHandler clientHandler : clientHandlers){
            try{
                if (!clientHandler.clienUsername.equals(clientHandler)){
                    clientHandler.bufferedWriter.write(s);
                    clientHandler.bufferedWriter.newLine();
                    clientHandler.bufferedWriter.flush();
                }
            } catch (Exception e) {
                closeEverything(socket,bufferedReader,bufferedWriter);
            }
        }
    }

    public void removeClienthandler(){
        broadcastMessage("Server: "+clienUsername+" left the chat");
        clientHandlers.remove(this);
    }


    private void closeEverything(Socket socket, BufferedReader bufferedReader, BufferedWriter bufferedWriter) {

        try{
            if (bufferedReader != null){
                bufferedReader.close();
            }
            if (bufferedWriter != null){
                bufferedWriter.close();
            }
            if (socket != null){
                socket.close();
            }
            if (clientHandlers.isEmpty() && Server.mensajesEnviados.isEmpty()){
                System.out.println("no se ha dicho nada");
                System.out.println(Server.mensajesEnviados.size());
            }else if (clientHandlers.isEmpty()) {
                for (int i = 0; i < Server.mensajesEnviados.size(); i++) {
                    System.out.println("Mensaje de "+Server.mensajesEnviados.get(i).getUsername()+": "+Server.mensajesEnviados.get(i).getMensaje());
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
