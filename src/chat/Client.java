package chat;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    private Socket socket;
    private BufferedReader bufferedReader;
    private BufferedWriter bufferedWriter;
    private String username;
    private String usuarioreceptor;

    public Client(Socket socket, String username) {
        try {
            this.socket = socket;
            this.username = username;
            this.bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        } catch (IOException e) {
            closeEverything();
        }
    }

    public void sendaMessage(){
        try{
            bufferedWriter.write(username);
            bufferedWriter.newLine();
            bufferedWriter.flush();

            Scanner scaner = new Scanner(System.in);
            while(socket.isConnected()){
                String messageToSend = scaner.nextLine();
                if (messageToSend.equals("adios")){
                    closeEverything();
                }else{
                    bufferedWriter.write(messageToSend);
                    bufferedWriter.newLine();
                    bufferedWriter.flush();
                }
            }
        } catch (IOException e) {
            closeEverything();
//            e.printStackTrace();
        }
    }

    public void listenForMessage(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                 String mensaje;
                 while (socket.isConnected()){
                     try{
                         mensaje = bufferedReader.readLine();
                         System.out.println(mensaje);
                     } catch (IOException e) {
                         closeEverything();
                     }
                 }
            }
        }).start();
    }

    private void closeEverything() {
        try{
//            if (bufferedReader != null){
                bufferedReader.close();
//            }
//            if (bufferedWriter != null){
                bufferedWriter.close();
//            }
//            if (socket != null){
                socket.close();
//            }
            System.exit(1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        try {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Dime tu nombre");
            String username = scanner.nextLine();
            Socket socket = new Socket("localhost",9090);
            Client client = new Client(socket,username);
            client.listenForMessage();
            client.sendaMessage();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
