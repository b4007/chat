package chat;

public class MensajesEnviados {
    private String username;
    private String mensaje;

    public MensajesEnviados(String username, String mensaje) {
        this.username = username;
        this.mensaje = mensaje;
    }

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public String getMensaje() {
        return mensaje;
    }
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
